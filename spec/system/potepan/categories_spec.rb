require 'rails_helper'

RSpec.feature "Categories" do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon) }
  let!(:product) { create(:product, taxons: [taxon]) }

  before do
    visit potepan_category_path(taxon.id)
  end

  describe "ページ全体" do
    it "正しいレイアウトが表示される" do
      expect(page).to have_title "#{taxon.name} - BIGBAG Store"
      expect(page).to have_selector('h2', text: taxon.name)
    end
  end

  describe "カテゴリーに紐づく商品" do
    it "カテゴリー別の商品名をクリックすると商品詳細ページにリンクする" do
      expect(page).to have_content product.name
      click_link product.name
      expect(current_path).to eq potepan_product_path(product.id)
    end

    it "カテゴリー別の商品価格をクリックすると商品詳細ページにリンクする" do
      expect(page).to have_content product.display_price
      click_link product.display_price
      expect(current_path).to eq potepan_product_path(product.id)
    end
  end

  describe "カテゴリーページのサイドバー" do
    it "サイドバーにカテゴリーが表示され、クリックすると詳細ページにリンクする" do
      within ".sideBar" do
        expect(page).to have_selector('li', text: taxonomy.name)
        expect(page).to have_selector('li', text: taxon.name)
        click_link taxon.name
        expect(current_path).to eq potepan_category_path(taxon.id)
      end
    end
  end
end
