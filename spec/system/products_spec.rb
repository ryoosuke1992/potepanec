require 'rails_helper'

RSpec.feature "Product" do
  let!(:taxon) { create(:taxon) }
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:related_product) { create(:product, taxons: [taxon]) }
  let!(:norelated_product) { create(:product) }

  it "商品詳細ページ" do
    # 商品詳細画面を開く
    visit potepan_product_path(product.id)

    # ページタイトルとh2の見出しタイトルが正しく表示される
    expect(page).to have_title "#{product.name} - BIGBAG Store"
    expect(page).to have_selector('h2', text: product.name)

    # 関連商品の商品名と価格が表示される
    expect(page).to have_content related_product.name
    expect(page).to have_content related_product.display_price

    # 関連しない商品は表示されない
    expect(page).not_to have_content norelated_product.name

    # 関連商品の商品名をクリックすると、詳細ページにリンクする
    within ".productsContent" do
      click_link related_product.name
      expect(current_path).to eq potepan_product_path(related_product.id)
    end

    # 関連商品の詳細ページの表示が正しいことを検証
    expect(page).to have_title "#{related_product.name} - BIGBAG Store"
    expect(page).to have_selector('h2', text: related_product.name)
  end
end
