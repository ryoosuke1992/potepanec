require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  describe "releted_products" do
    subject { product.releted_products }

    let!(:taxon) { create(:taxon) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:related_product) { create(:product, taxons: [taxon]) }
    let!(:norelated_product) { create(:product) }

    it { is_expected.to include related_product }
    it { is_expected.not_to include norelated_product }
    it { is_expected.not_to include product }
  end
end
