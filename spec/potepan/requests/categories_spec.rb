require 'rails_helper'

RSpec.describe "Categories", type: :request do
  describe "categoriesページにアクセスした時" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:product1) { create(:product) }

    before do
      get potepan_category_path(taxon.id)
    end

    it "レスポンスが正常に表示されること" do
      expect(response).to be_successful
      expect(response).to have_http_status(200)
    end

    it "商品カテゴリー名が表示されていること" do
      expect(response.body).to include taxon.name
    end

    it "カテゴリー名が表示されていること" do
      expect(response.body).to include taxonomy.name
    end

    it "カテゴリーに含まれる商品名が表示される" do
      expect(response.body).to include product.name
    end

    it "カテゴリーに含まれない商品名は表示されない" do
      expect(response.body).not_to include product1.name
    end
  end
end
