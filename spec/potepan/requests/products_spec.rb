require 'rails_helper'
require 'spree/testing_support/factories'

RSpec.describe "商品", type: :request do
  let!(:taxon) { create(:taxon) }
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

  before do
    get potepan_product_path(product.id)
  end

  context '商品が存在する場合' do
    it 'リクエストが成功すること' do
      expect(response).to be_successful
      expect(response.status).to eq 200
    end
  end

  context '関連商品が存在する場合' do
    it "関連商品が4件取得される" do
      expect(Capybara.string(response.body)).to have_selector ".productBox", count: 4
    end
  end
end
