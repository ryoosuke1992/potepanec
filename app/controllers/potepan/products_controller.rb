class Potepan::ProductsController < ApplicationController
  def show
    @product = Spree::Product.find(params[:id])
    @related_products = @product.releted_products.includes(master: [:default_price, :images]).limit(LIMIT_RELATED_PRODUCT)
  end
end
